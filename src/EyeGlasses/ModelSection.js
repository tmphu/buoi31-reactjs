import React, { Component } from "react";

export default class ModelSection extends Component {
  render() {
    return (
      <div className="row">
        <div
          className="col-6"
          style={{
            position: "relative",
          }}
        >
          <div style={{ display: this.props.pickedImage.display }}>
            <img
              src={this.props.pickedImage.url}
              alt=""
              className="sunGlasses"
            />
            <div className="body-desc">
              <h2
                style={{
                  color: "#4E0E78",
                  fontSize: "18px",
                }}
              >
                {this.props.pickedImage.name}
              </h2>
              <p style={{ color: "#fff", fontSize: "12px" }}>
                {this.props.pickedImage.desc}
              </p>
            </div>
          </div>
          <img src="./img/model.jpg" alt="" style={{ height: "300px" }} />
        </div>
        <div className="col-6">
          <img src="./img/model.jpg" alt="" style={{ height: "300px" }} />
        </div>
      </div>
    );
  }
}
