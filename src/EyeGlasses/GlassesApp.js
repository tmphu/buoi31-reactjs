import React, { Component } from "react";
import GlassesCollection from "./GlassesCollection";
import ModelSection from "./ModelSection";

export default class GlassesApp extends Component {
  state = {
    img: {
      url: "",
      name: "",
      desc: "",
      display: "none",
    },
  };

  handleChangeGlasses = (pickedGlasses, displayState) => {
    this.setState({
      img: {
        url: pickedGlasses.url,
        name: pickedGlasses.name,
        desc: pickedGlasses.desc,
        display: displayState,
      },
    });
  };

  render() {
    return (
      <div id="main">
        <div className="layer">
          <div className="heading">
            <h2>TRY GLASSES APP ONLINE</h2>
          </div>
          <div className="container py-5">
            <ModelSection pickedImage={this.state.img} />
            <GlassesCollection handleChangeGlasses={this.handleChangeGlasses} />
          </div>
        </div>
      </div>
    );
  }
}
