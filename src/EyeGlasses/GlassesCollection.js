import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";

export default class GlassesCollection extends Component {
  renderItem() {
    return dataGlasses.map((item, index) => {
      return (
        <div key={index} className="col-2">
          <img
            onMouseOver={() => {
              this.props.handleChangeGlasses(item, "inline-block");
            }}
            src={item.url}
            alt=""
            style={{
              width: "80px",
              borderStyle: "solid",
              borderColor: "#adadad",
            }}
          />
        </div>
      );
    });
  }
  render() {
    return (
      <div
        className="row mx-auto my-5 px-5 py-5 bg-white"
        style={{ width: "90%" }}
      >
        {this.renderItem()}
      </div>
    );
  }
}
