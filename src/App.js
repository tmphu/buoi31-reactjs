import logo from "./logo.svg";
import GlassesApp from "./EyeGlasses/GlassesApp";
import "./App.css";

function App() {
  return (
    <div className="App">
      <GlassesApp />
    </div>
  );
}

export default App;
